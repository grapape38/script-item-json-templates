package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func equalsLoosely(s1 string, s2 string) bool {
	lowerConcat := func(s string) string { return strings.ToLower(strings.Join(strings.Fields(s), "")) }
	return lowerConcat(s1) == lowerConcat(s2)
}

func makeCamelCase(s string, upperInitial bool) string {
	words := strings.Fields(s)
	for i := range words {
		if i != 0 || upperInitial {
			words[i] = strings.ToUpper(words[i][:1]) + words[i][1:]
		} else if i == 0 && !upperInitial {
			words[i] = strings.ToLower(words[i][:1]) + words[i][1:]
		}
	}
	return strings.Join(words, "")
}

func replaceCommonSymbols(s string) string {
	s = strings.ReplaceAll(s, "%", "Percent")
	s = strings.ReplaceAll(s, string(rune(0x03B1)), "Alpha")
	return s
}

func removeNonAlphaNumOrSpace(s string) string {
	var ret []byte
	for _, b := range []byte(s) {
		if b == ' ' || (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z') || (b >= '0' && b <= '9') {
			ret = append(ret, b)
		}
	}
	return string(ret)
}

func cleanScriptString(s string) string {
	return removeNonAlphaNumOrSpace(replaceCommonSymbols(s))
}

func skipWord(s string, word string) string {
	words := strings.Fields(s)
	if len(words) == 0 {
		return ""
	}
	if equalsLoosely(word, words[0]) {
		words = words[1:]
	}
	return strings.Join(words, " ")
}

func nameLookup(names []string, name string) int {
	for index, n := range names {
		if equalsLoosely(n, name) {
			return index
		}
	}
	return -1
}

func stripBOM(b []byte) []byte {
	if len(b) >= 3 {
		if b[0] == 0xEF && b[1] == 0xBB && b[2] == 0xBF {
			return b[3:]
		}
	}
	return b
}

func formatJSONError(input string, err error) string {
	if jsonError, ok := err.(*json.SyntaxError); ok {
		line, character, lcErr := lineAndCharacter(input, int(jsonError.Offset))
		if lcErr != nil {
			return fmt.Sprintf("Couldn't find the line and character position of the error due to error %v\n", lcErr)
		}
		return fmt.Sprintf("Cannot parse JSON schema due to a syntax error at line %d, character %d: %v\n", line, character, jsonError.Error())
	}
	if jsonError, ok := err.(*json.UnmarshalTypeError); ok {
		line, character, lcErr := lineAndCharacter(input, int(jsonError.Offset))
		if lcErr != nil {
			return fmt.Sprintf("Couldn't find the line and character position of the error due to error %v\n", lcErr)
		}
		return fmt.Sprintf("The JSON type '%v' cannot be converted into the Go '%v' type on struct '%s', field '%v'. See input file line %d, character %d\n", jsonError.Value, jsonError.Type.Name(), jsonError.Struct, jsonError.Field, line, character)
	}
	if err != nil {
		return fmt.Sprintf("JSON parsing error: %v\n", err)
	}
	return ""
}

func lineAndCharacter(input string, offset int) (line int, character int, err error) {
	lf := rune(0x0A)

	if offset > len(input) || offset < 0 {
		return 0, 0, fmt.Errorf("couldn't find offset %d within the input", offset)
	}

	// Humans tend to count from 1.
	line = 1

	for i, b := range input {
		if b == lf {
			line++
			character = 0
		}
		character++
		if i == offset {
			break
		}
	}

	return line, character, nil
}
