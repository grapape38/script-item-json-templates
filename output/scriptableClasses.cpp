enum graphBuilderOpcodes {
    EGraphBuilderGraphBuilderCmd,
    EGraphBuilderShowControlPanelCmd,
    EGraphBuilderLegendPositionCmd,
    EGraphBuilderLegendFloatingOffsetCmd,
    EGraphBuilderLegendSettingsCmd,
    EGraphBuilderTitleAlignmentCmd,
    EGraphBuilderTitleSpanCmd,
    EGraphBuilderSubtitleAlignmentCmd,
    EGraphBuilderSubtitleSpanCmd,
    EGraphBuilderLockScalesCmd,
    EGraphBuilderErrorBarOffsetCmd,
    EGraphBuilderLinkPageAxesCmd,
    EGraphBuilderFitToWindowCmd,
    EGraphBuilderAutoStretchingCmd,
    EGraphBuilderSamplingCmd,
    EGraphBuilderGraphSpacingCmd,
    EGraphBuilderPageGapSizeCmd,
    EGraphBuilderPageCountLimitCmd,
    EGraphBuilderIncludeMissingCategoriesCmd,
    EGraphBuilderShowExcludedRowsCmd,
    EGraphBuilderLaunchAnalysisCmd,
    EGraphBuilderMakeIntoDataTableCmd,
    EGraphBuilderRandomSeedCmd,
    EGraphBuilderContinuousPointsLimitCmd,
    EGraphBuilderContinuousAlternateCmd,
    EGraphBuilderContinuousSmootherCmd,
    EGraphBuilderSmoothingParameterCmd,
    EGraphBuilderCategoricalPointsLimitCmd,
    EGraphBuilderCategoricalAlternateCmd,
    EGraphBuilderJitterCmd,
    EGraphBuilderMissingShapesCmd,
    EGraphBuilderGraphWidthCmd,
    EGraphBuilderGraphHeightCmd,
    EGraphBuilderXGroupEdgeCmd,
    EGraphBuilderYGroupEdgeCmd,
    EGraphBuilderYGroupLevelOrientationCmd,
    EGraphBuilderYGroupTitleOrientationCmd,
    EGraphBuilderSummaryStatisticCmd,
    EGraphBuilderOrderStatisticCmd,
    EGraphBuilderBackColorCmd,
    EGraphBuilderGridColorCmd,
    EGraphBuilderGridTransparencyCmd,
    EGraphBuilderSpacingBordersCmd,
    EGraphBuilderTitleFillColorCmd,
    EGraphBuilderTitleTextColorCmd,
    EGraphBuilderTitleFrameColorCmd,
    EGraphBuilderTitleTransparencyCmd,
    EGraphBuilderTitleUnderlineCmd,
    EGraphBuilderLevelFillColorCmd,
    EGraphBuilderLevelTextColorCmd,
    EGraphBuilderLevelFrameColorCmd,
    EGraphBuilderLevelTransparencyCmd,
    EGraphBuilderLevelSpacingColorCmd,
    EGraphBuilderLevelSpacingTransparencyCmd,
    EGraphBuilderLevelUnderlineCmd,
    EGraphBuilderPageLevelFillColorCmd,
    EGraphBuilderPageLevelTextColorCmd,
    EGraphBuilderPageLevelFrameColorCmd,
    EGraphBuilderPageLevelTransparencyCmd,
    EGraphBuilderPageLevelUnderlineCmd,
    EGraphBuilderRelativeSizesCmd,
    EGraphBuilderGetLegendServerCmd,
    EGraphBuilderGetLegendDisplayCmd,
    EGraphBuilderDoneCmd,
    EGraphBuilderGraphBuilderCmd,
    EGraphBuilderShowTitleCmd,
    EGraphBuilderShowSubtitleCmd,
    EGraphBuilderShowLegendCmd,
    EGraphBuilderShowFooterCmd,
    EGraphBuilderShowXAxisCmd,
    EGraphBuilderShowYAxisCmd,
    EGraphBuilderShowXAxisTitleCmd,
    EGraphBuilderShowYAxisTitleCmd,
    EGraphBuilderTitleCmd,
    EGraphBuilderSubtitleCmd,
    EGraphBuilderLegendCmd,
    EGraphBuilderFooterCmd,
    EGraphBuilderXAxisCmd,
    EGraphBuilderYAxisCmd,
    EGraphBuilderXAxisTitleCmd,
    EGraphBuilderYAxisTitleCmd,
    EGraphBuilderContinuousColorThemeCmd,
    EGraphBuilderCategoricalColorThemeCmd,
    EGraphBuilderLightenLargeFillsCmd,
    EGraphBuilderUseRowColorsForLevelsCmd
};

static JString const graphBuilderLegendPositionNames = {
    "Right",
    "Bottom",
    "Inside Left",
    "Inside Right",
    "Inside Bottom Left",
    "Inside Bottom Right",
    "Inside Floating",
    JString::null()
};

enum class ELegendPosition {
    Right,
    Bottom,
    InsideLeft,
    InsideRight,
    InsideBottomLeft,
    InsideBottomRight,
    InsideFloating,
    N
};

static JString const graphBuilderTitleAlignmentNames = {
    "Left",
    "Center",
    "Right",
    JString::null()
};

enum class ETitleAlignment {
    Left,
    Center,
    Right,
    N
};

static JString const graphBuilderTitleSpanNames = {
    "Full",
    "Graph contents",
    JString::null()
};

enum class ETitleSpan {
    Full,
    GraphContents,
    N
};

static JString const graphBuilderSubtitleAlignmentNames = {
    "Left",
    "Center",
    "Right",
    "Auto",
    JString::null()
};

enum class ESubtitleAlignment {
    Left,
    Center,
    Right,
    Auto,
    N
};

static JString const graphBuilderSubtitleSpanNames = {
    "Full",
    "Graph contents",
    JString::null()
};

enum class ESubtitleSpan {
    Full,
    GraphContents,
    N
};

static JString const graphBuilderLinkPageAxesNames = {
    "None",
    "X Only",
    "Y Only",
    "X and Y",
    JString::null()
};

enum class ELinkPageAxes {
    None,
    XOnly,
    YOnly,
    XAndY,
    N
};

static JString const graphBuilderFitToWindowNames = {
    "Auto",
    "On",
    "Off",
    "Maintain Aspect Ratio",
    JString::null()
};

enum class EFitToWindow {
    Auto,
    On,
    Off,
    MaintainAspectRatio,
    N
};

static JString const graphBuilderContinuousAlternateNames = {
    "None",
    "Contour",
    JString::null()
};

enum class EContinuousAlternate {
    None,
    Contour,
    N
};

static JString const graphBuilderCategoricalAlternateNames = {
    "None",
    "Contour",
    "Line",
    "Smoother",
    "Box Plot",
    "Bar",
    "Histogram",
    JString::null()
};

enum class ECategoricalAlternate {
    None,
    Contour,
    Line,
    Smoother,
    BoxPlot,
    Bar,
    Histogram,
    N
};

static JString const graphBuilderXGroupEdgeNames = {
    "Top",
    "Bottom",
    JString::null()
};

enum class EXGroupEdge {
    Top,
    Bottom,
    N
};

static JString const graphBuilderYGroupEdgeNames = {
    "Left",
    "Right",
    JString::null()
};

enum class EYGroupEdge {
    Left,
    Right,
    N
};

static JString const graphBuilderYGroupLevelOrientationNames = {
    "Horizontal",
    "Vertical",
    JString::null()
};

enum class EYGroupLevelOrientation {
    Horizontal,
    Vertical,
    N
};

static JString const graphBuilderYGroupTitleOrientationNames = {
    "Horizontal",
    "Vertical",
    JString::null()
};

enum class EYGroupTitleOrientation {
    Horizontal,
    Vertical,
    N
};

static JString const graphBuilderSummaryStatisticNames = {
    "N",
    "Mean",
    "Median",
    "Geometric Mean",
    "Min",
    "Max",
    "Range",
    "Sum",
    "Cumulative Sum",
    "% of Total",
    "% of Factor",
    "% of Grand Total",
    "Std Dev",
    "Variance",
    "Std Err",
    "Interquartile Range",
    "Median Absolute Deviation",
    "First Quartile",
    "Third Quartile",
    JString::null()
};

enum class ESummaryStatistic {
    N,
    Mean,
    Median,
    GeometricMean,
    Min,
    Max,
    Range,
    Sum,
    CumulativeSum,
    PercentOfTotal,
    PercentOfFactor,
    PercentOfGrandTotal,
    StdDev,
    Variance,
    StdErr,
    InterquartileRange,
    MedianAbsoluteDeviation,
    FirstQuartile,
    ThirdQuartile,
    N
};

static JString const graphBuilderOrderStatisticNames = {
    "N",
    "Mean",
    "Median",
    "Geometric Mean",
    "Min",
    "Max",
    "Range",
    "Sum",
    "Cumulative Sum",
    "% of Total",
    "% of Factor",
    "% of Grand Total",
    "Std Dev",
    "Variance",
    "Std Err",
    "Interquartile Range",
    "Median Absolute Deviation",
    "First Quartile",
    "Third Quartile",
    JString::null()
};

enum class EOrderStatistic {
    N,
    Mean,
    Median,
    GeometricMean,
    Min,
    Max,
    Range,
    Sum,
    CumulativeSum,
    PercentOfTotal,
    PercentOfFactor,
    PercentOfGrandTotal,
    StdDev,
    Variance,
    StdErr,
    InterquartileRange,
    MedianAbsoluteDeviation,
    FirstQuartile,
    ThirdQuartile,
    N
};


static ScriptItem const* showItems = {
    ScriptItem("Show Title", SI_BoolProp, SI_NoMenu | SI_Dflt, EShowShowTitleCmd)
        .tip("Displays or hides the graph title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Title( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Title(0);"),
    ScriptItem("Show Subtitle", SI_BoolProp, SI_NoMenu, EShowShowSubtitleCmd)
        .tip("Displays or hides the graph subtitle.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Subtitle( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Subtitle(1);"),
    ScriptItem("Show Legend", SI_BoolProp, SI_NoMenu | SI_Dflt, EShowShowLegendCmd)
        .tip("Displays or hides the legend to the right of the graph.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Legend( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Legend(1);"),
    ScriptItem("Show Footer", SI_BoolProp, SI_NoMenu | SI_Dflt, EShowShowFooterCmd)
        .tip("Displays or hides the footer text.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Footer( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Footer(0);"),
    ScriptItem("Show X Axis", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoPref, EShowShowXAxisCmd)
        .tip("Displays or hides the X axis.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show X Axis( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show X Axis(0);"),
    ScriptItem("Show Y Axis", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoPref, EShowShowYAxisCmd)
        .tip("Displays or hides the Y axis.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Y Axis( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Y Axis(0);"),
    ScriptItem("Show X Axis Title", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoPref, EShowShowXAxisTitleCmd)
        .tip("Displays or hides the X axis title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show X Axis Title( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show X Axis Title(0);"),
    ScriptItem("Show Y Axis Title", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoPref, EShowShowYAxisTitleCmd)
        .tip("Displays or hides the Y axis title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Y Axis Title( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Y Axis Title(0);"),
    ScriptItem("Title", SI_BoolProp, SI_MenuOnly, EShowTitleCmd)
        .tip("Displays or hides the graph title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Title( state=0|1 );"),
    ScriptItem("Subtitle", SI_BoolProp, SI_MenuOnly, EShowSubtitleCmd)
        .tip("Displays or hides the graph subtitle.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Subtitle( state=0|1 );"),
    ScriptItem("Legend", SI_BoolProp, SI_MenuOnly, EShowLegendCmd)
        .tip("Displays or hides the legend to the right of the graph.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Legend( state=0|1 );"),
    ScriptItem("Footer", SI_BoolProp, SI_MenuOnly, EShowFooterCmd)
        .tip("Displays or hides the footer text.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Footer( state=0|1 );"),
    ScriptItem("X Axis", SI_BoolProp, SI_MenuOnly, EShowXAxisCmd)
        .tip("Displays or hides the X axis.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; X Axis( state=0|1 );"),
    ScriptItem("Y Axis", SI_BoolProp, SI_MenuOnly, EShowYAxisCmd)
        .tip("Displays or hides the Y axis.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Y Axis( state=0|1 );"),
    ScriptItem("X Axis Title", SI_BoolProp, SI_MenuOnly, EShowXAxisTitleCmd)
        .tip("Displays or hides the X axis title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; X Axis Title( state=0|1 );"),
    ScriptItem("Y Axis Title", SI_BoolProp, SI_MenuOnly, EShowYAxisTitleCmd)
        .tip("Displays or hides the Y axis title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Y Axis Title( state=0|1 );"),
    ScriptItem::EndOfTable
};

static ScriptItem const* colorSettingsItems = {
    ScriptItem("Continuous Color Theme...", SI_Action, SI_NoPref, EColorSettingsContinuousColorThemeCmd)
        .tip("Sets the color theme used for gradients.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Continuous Color Theme;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Continuous Color Theme(&#34;White to Black&#34;);"),
    ScriptItem("Categorical Color Theme...", SI_Action, SI_NoPref, EColorSettingsCategoricalColorThemeCmd)
        .tip("Sets the color theme used for categories.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Categorical Color Theme;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Categorical Color Theme(&#34;Pastel&#34;);"),
    ScriptItem("Lighten large fills", SI_BoolProp, SI_Dflt, EColorSettingsLightenLargeFillsCmd)
        .tip("Automatically lighten colors for pie, treemap and mosaic elements, which fill large areas.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Lighten large fills( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Lighten large fills(1);"),
    ScriptItem("Use row colors for levels", SI_BoolProp, SI_Dflt, EColorSettingsUseRowColorsForLevelsCmd)
        .tip("Initialize legend levels with row colors when every level has a unique color.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Use row colors for levels( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Use row colors for levels(1);"),
    ScriptItem::EndOfTable
};

static ScriptItem const* graphBuilderItems = {
    ScriptItem("Graph Builder", , SI_NoMenu | SI_NoScript | SI_NoPref, EGraphBuilderGraphBuilderCmd)
        .doc("Provides an interactive graphical interface that enables you to explore your data. You can drag columns into graph zones to create a variety of graphs including scatterplots, contour plots, bar charts, area charts, box plots, histograms, heat maps, pie charts, treemaps, mosaic plots, and maps.")
        .helpKey("Graph Builder")
        .prototype("Graph Builder( Variables( X(column ), Y( column ), &lt;Group X( column )&gt;, &lt;Group Y( column )&gt;, &lt;Shape( column )&gt;, &lt;Color( column )&gt;, &lt;Overlay( column )&gt;, &lt;Freq( column )&gt; ), &lt;Elements(...)&gt; ) );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Sex ), Y( :Height ), Group X( :Age ) ), Elements(Box Plot(X,Y)));"),
    ScriptItem("Show Control Panel", SI_BoolProp, SI_Dflt | SI_NoPref, EGraphBuilderShowControlPanelCmd)
        .tip("Displays or hides the control panel.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Control Panel( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Show Control Panel(1);"),
    ScriptItem("Show", Show),
    ScriptItem("Legend Position", SI_EnumProperty, 0, EGraphBuilderLegendPositionCmd, graphBuilderLegendPositionNames)
        .doc("Sets the position of the legend.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Legend Position( &#34;Right&#34;|&#34;Bottom&#34;|&#34;Inside Left&#34;|&#34;Inside Right&#34;|&#34;Inside Bottom Left&#34;|&#34;Inside Bottom Right&#34;|&#34;Inside Floating&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Legend Position(&#34;Bottom&#34;);"),
    ScriptItem("Legend Floating Offset", SI_TopExpr, SI_NoMenu | SI_NoPref, EGraphBuilderLegendFloatingOffsetCmd)
        .doc("Sets the offset in pixels for the legend when the Legend Position is set to &#34;Floating&#34;")
        .prototype("obj &lt;&lt; Legend Floating Offset;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Legend Position(&#34;Inside Floating&#34;);"),
    ScriptItem("Legend Settings...", SI_Action, SI_NoPref, EGraphBuilderLegendSettingsCmd)
        .tip("Opens a dialog to modify the properties of the legend.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Legend Settings;")
        .example("^LAUNCH_EXAMPLE; wait(1); obj &lt;&lt; Legend Settings();"),
    ScriptItem("Color Settings", Color Settings),
    ScriptItem("Title Alignment", SI_EnumProperty, SI_NoMenu, EGraphBuilderTitleAlignmentCmd, graphBuilderTitleAlignmentNames)
        .doc("Sets the alignment of the graph title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Title Alignment( &#34;Left&#34;|&#34;Center&#34;|&#34;Right&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Alignment(&#34;Left&#34;);"),
    ScriptItem("Title Span", SI_EnumProperty, SI_NoMenu, EGraphBuilderTitleSpanCmd, graphBuilderTitleSpanNames)
        .doc("Sets the span of the graph title.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Title Span( &#34;Full&#34;|&#34;Graph contents&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Span(&#34;Graph&#34;);"),
    ScriptItem("Subtitle Alignment", SI_EnumProperty, SI_NoMenu, EGraphBuilderSubtitleAlignmentCmd, graphBuilderSubtitleAlignmentNames)
        .doc("Sets the alignment of the graph subtitle.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Subtitle Alignment( &#34;Left&#34;|&#34;Center&#34;|&#34;Right&#34;|&#34;Auto&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Subtitle Alignment(&#34;Left&#34;);"),
    ScriptItem("Subtitle Span", SI_EnumProperty, SI_NoMenu, EGraphBuilderSubtitleSpanCmd, graphBuilderSubtitleSpanNames)
        .doc("Sets the span of the graph subtitle.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Subtitle Span( &#34;Full&#34;|&#34;Graph contents&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Subtitle Span(&#34;Graph&#34;);"),
    ScriptItem("Lock Scales", SI_BoolProp, 0, EGraphBuilderLockScalesCmd)
        .tip("Locks axis and gradient ranges so they do not change in response to data or filtering changes.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Lock Scales( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Lock Scales(1);"),
    ScriptItem("Error Bar Offset...", SI_Action, SI_NoPref, EGraphBuilderErrorBarOffsetCmd)
        .tip("Opens a dialog to set the offset for error bars.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Error Bar Offset;")
        .example("^LAUNCH_EXAMPLE; wait(1); obj &lt;&lt; Error Bar Offset(0.01);"),
    ScriptItem("Link Page Axes", SI_EnumProperty, 0, EGraphBuilderLinkPageAxesCmd, graphBuilderLinkPageAxesNames)
        .tip("Sets which axes are linked across page group levels.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Link Page Axes( &#34;None&#34;|&#34;X Only&#34;|&#34;Y Only&#34;|&#34;X and Y&#34; );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder(Size(470, 552),Variables(X(:height), Y(:weight), Page(:sex)),	Elements(Points(X, Y), Smoother(X, Y))); obj &lt;&lt; Link Page Axes(&#34;Y Only&#34;);"),
    ScriptItem("Fit to Window", SI_EnumProperty, 0, EGraphBuilderFitToWindowCmd, graphBuilderFitToWindowNames)
        .tip("Sets the auto stretching behavior of the report.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Fit to Window( &#34;Auto&#34;|&#34;On&#34;|&#34;Off&#34;|&#34;Maintain Aspect Ratio&#34; );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Fit to Window(&#34;Off&#34;);"),
    ScriptItem("Auto Stretching", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoPref, EGraphBuilderAutoStretchingCmd)
        .doc("Toggles the automatic stretching of the graph with the containing window.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Auto Stretching( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Auto Stretching(0);"),
    ScriptItem("Sampling...", SI_NumProp, 0, EGraphBuilderSamplingCmd)
        .tip("Randomly selects a sample of the data of the size specified. This is useful when the data is large and the graph is still being changed.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Sampling( number );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Sampling(20);"),
    ScriptItem("Graph Spacing...", SI_NumProp, 0, EGraphBuilderGraphSpacingCmd)
        .tip("Sets the amount of space between graph panels.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Graph Spacing( gap=1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Graph Spacing(3);"),
    ScriptItem("Page Gap Size", SI_NumProp, 0, EGraphBuilderPageGapSizeCmd)
        .tip("Sets the amount of space between page groups.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Gap Size( gap=25 );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Gap Size(3);"),
    ScriptItem("Page Count Limit", SI_NumProp, SI_NoMenu, EGraphBuilderPageCountLimitCmd)
        .tip("Sets the maximum number of pages created for the page variable, to avoid accidental performance degradation.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Page Count Limit( count=200 );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Name ) ), Elements(Points(X,Y))); obj &lt;&lt; Page Count Limit(5);"),
    ScriptItem("Include Missing Categories", SI_BoolProp, 0, EGraphBuilderIncludeMissingCategoriesCmd)
        .tip("Treats missing values as a separate level for categorical variables.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Include Missing Categories( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; :age[{10, 20, 30}] = .; obj &lt;&lt; Include Missing Categories(1);"),
    ScriptItem("Set α Level", , SI_NoScript, EGraphBuilderSetAlphaLevelCmd)
        .doc("Changes the alpha level used for the confidence curves.")
        .prototype("obj &lt;&lt; Set α Level( &#34;0.10&#34;|&#34;0.05&#34;|&#34;0.01&#34;|&#34;Other…&#34;=0.05 );"),
    ScriptItem("Set Alpha Level,Set α Level", , SI_NoMenu | SI_NoPref, EGraphBuilderSetAlphaLevelSetAlphaLevelCmd)
        .doc("Changes the alpha level used for the confidence curves.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Set Alpha Level,Set α Level( 0.10|0.05|0.01|Other... );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Set Alpha Level(0.10);"),
    ScriptItem("Show Excluded Rows", SI_BoolProp, 0, EGraphBuilderShowExcludedRowsCmd)
        .tip("Shows excluded rows for grouping and raw points, but still excludes them from calculations and some graphic elements.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Show Excluded Rows( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; dt &lt;&lt; Select Rows(1::5); dt &lt;&lt; Exclude(); obj &lt;&lt; Show Excluded Rows(1);"),
    ScriptItem("Launch Analysis", SI_Action, SI_NoPref, EGraphBuilderLaunchAnalysisCmd)
        .tip("Launches an analysis using the current variables.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Launch Analysis;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Launch Analysis;"),
    ScriptItem("Make into Data Table", SI_Action, SI_NoPref, EGraphBuilderMakeIntoDataTableCmd)
        .tip("Creates a new data table containing images of graphs.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Make into Data Table;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Make into Data Table;"),
    ScriptItem("Random Seed", SI_NumProp, SI_NoMenu | SI_NoPref, EGraphBuilderRandomSeedCmd)
        .tip("Sets a specific seed for random jitter.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Random Seed( number );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;);obj = dt &lt;&lt; Graph Builder(Variables(X(:Sex), Y(:Height)), Elements(Points(X, Y, Jitter(&#34;Random Uniform&#34;))));wait(1);obj &lt;&lt; Random Seed(123456);"),
    ScriptItem("Continuous Points Limit", SI_NumProp, SI_NoMenu | SI_NoScript, EGraphBuilderContinuousPointsLimitCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Continuous Points Limit( number=50000 );"),
    ScriptItem("Continuous Alternate", SI_EnumProperty, SI_NoMenu | SI_NoScript, EGraphBuilderContinuousAlternateCmd, graphBuilderContinuousAlternateNames)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Continuous Alternate( &#34;None&#34;|&#34;Contour&#34; );"),
    ScriptItem("Continuous Smoother", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoScript, EGraphBuilderContinuousSmootherCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Continuous Smoother( state=0|1 );"),
    ScriptItem("Smoothing Parameter", SI_NumProp, SI_NoMenu | SI_NoScript, EGraphBuilderSmoothingParameterCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Smoothing Parameter( number=0.05 );"),
    ScriptItem("Categorical Points Limit", SI_NumProp, SI_NoMenu | SI_NoScript, EGraphBuilderCategoricalPointsLimitCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Categorical Points Limit( number=5000 );"),
    ScriptItem("Categorical Alternate", SI_EnumProperty, SI_NoMenu | SI_NoScript, EGraphBuilderCategoricalAlternateCmd, graphBuilderCategoricalAlternateNames)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Categorical Alternate( &#34;None&#34;|&#34;Contour&#34;|&#34;Line&#34;|&#34;Smoother&#34;|&#34;Box Plot&#34;|&#34;Bar&#34;|&#34;Histogram&#34;=&#34;Box Plot&#34; );"),
    ScriptItem("Jitter", SI_BoolProp, SI_NoMenu | SI_Dflt | SI_NoScript, EGraphBuilderJitterCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Jitter( state=0|1 );"),
    ScriptItem("Missing Shapes", SI_BoolProp, SI_NoMenu | SI_NoScript, EGraphBuilderMissingShapesCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Missing Shapes( state=0|1 );"),
    ScriptItem("Graph Width", SI_NumProp, SI_NoMenu | SI_NoScript, EGraphBuilderGraphWidthCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Graph Width( number );"),
    ScriptItem("Graph Height", SI_NumProp, SI_NoMenu | SI_NoScript, EGraphBuilderGraphHeightCmd)
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Graph Height( number );"),
    ScriptItem("Size", , SI_NoMenu, EGraphBuilderSizeCmd)
        .doc("Sets the size of the graph.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Size( width, height );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Size(808, 586);"),
    ScriptItem("Variables", , SI_NoMenu | , EGraphBuilderVariablesCmd)
        .doc("Defines the variables used in the visualization.")
        .helpKey("Graph Builder")
        .prototype("Variables( X(column ), Y( column ), &lt;Group X( column )&gt;, &lt;Group Y( column )&gt;, &lt;Shape( column )&gt;, &lt;Color( column )&gt;, &lt;Overlay( column )&gt;, &lt;Freq( column )&gt; )")
        .example("dt = Open(&#34;$SAMPLE_DATA/Crime.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables(Color( :robbery ), Shape( :State ) ) );"),
    ScriptItem("Elements", , SI_NoMenu | , EGraphBuilderElementsCmd)
        .doc("Identifies the elements of the visualization.")
        .helpKey("Graph Builder Elements")
        .prototype("Elements( Points( X, Y )| Box plot( X, Y, Jitter( state=0|1 ), Outliers( state=0|1 ), Box Style( &#34;Outlier&#34;|&#34;Quantile&#34; ) )|Line( X, Y, Row Order( number ), Summary Statistic( ) )| Histogram( X, Y)| Bar( X, Y, Bar Style(), Summary Statistic() )| Contour(X, Y)| Smoother(X, Y)|Map Shapes(Summary Statistic() ))")
        .example("dt = Open(&#34;$SAMPLE_DATA/PopAgeGroup.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables(  X( :&#34;F Rate 0-19&#34;n ), Y( :Region ) ), Elements( Box Plot( X, Y ), Line( X, Y, Summary Statistic( &#34;Mean&#34; )  )) );"),
    ScriptItem("X Group Edge", SI_EnumProperty, SI_NoMenu, EGraphBuilderXGroupEdgeCmd, graphBuilderXGroupEdgeNames)
        .doc("Moves the axis for the X group to either the top or the bottom. &#34;Top&#34; by default.")
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; X Group Edge( &#34;Top&#34;|&#34;Bottom&#34; );")
        .example("^LAUNCH_EXAMPLE; wait(1); obj &lt;&lt; X Group Edge(&#34;Bottom&#34;);"),
    ScriptItem("Y Group Edge", SI_EnumProperty, SI_NoMenu, EGraphBuilderYGroupEdgeCmd, graphBuilderYGroupEdgeNames)
        .doc("Moves the axis for the Y group to either the left or the right. &#34;Right&#34; by default.")
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; Y Group Edge( &#34;Left&#34;|&#34;Right&#34; );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Sex ), Y( :Height ), Group Y( :Age ) ), Elements(Smoother(X,Y))); wait(1); obj &lt;&lt; Y Group Edge( &#34;Left&#34; );"),
    ScriptItem("Y Group Level Orientation", SI_EnumProperty, SI_NoMenu, EGraphBuilderYGroupLevelOrientationCmd, graphBuilderYGroupLevelOrientationNames)
        .doc("Determines whether the Y Group level label text is horizontal or vertical (rotated).")
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; Y Group Level Orientation( &#34;Horizontal&#34;|&#34;Vertical&#34; );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Sex ), Y( :Height ), Group Y( :Age ) ), Elements(Smoother(X,Y))); wait(1); obj &lt;&lt; Y Group Level Orientation( &#34;Horizontal&#34; );"),
    ScriptItem("Y Group Title Orientation", SI_EnumProperty, SI_NoMenu, EGraphBuilderYGroupTitleOrientationCmd, graphBuilderYGroupTitleOrientationNames)
        .doc("Determines whether the Y Group title label text is horizontal or vertical (rotated).")
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; Y Group Title Orientation( &#34;Horizontal&#34;|&#34;Vertical&#34; );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Sex ), Y( :Height ), Group Y( :Age ) ), Elements(Smoother(X,Y))); wait(1); obj &lt;&lt; Y Group Title Orientation( &#34;Horizontal&#34; );"),
    ScriptItem("Summary Statistic", SI_EnumProperty, SI_NoMenu, EGraphBuilderSummaryStatisticCmd, graphBuilderSummaryStatisticNames)
        .doc("Sets the default summary statistic used by the different elements in the graph. Mean by default for Bar and Line elements.")
        .helpKey("Graph Builder Elements")
        .prototype("Summary Statistic( N|Mean|Min|Max|Sum|% of Total )")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Y( :weight, Position( 1 ) ) ),Summary Statistic( &#34;Sum&#34; ),	Elements(Bar( X, Y( 1 ), Y( 2 ), Legend( 2 ))));"),
    ScriptItem("Order Statistic", SI_EnumProperty, SI_NoMenu, EGraphBuilderOrderStatisticCmd, graphBuilderOrderStatisticNames)
        .doc("Sets the default order based on a summary statistic used when using the Order By message for a variable in the graph.")
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; Order Statistic( &#34;N&#34;|&#34;Mean&#34;|&#34;Median&#34;|&#34;Geometric Mean&#34;|&#34;Min&#34;|&#34;Max&#34;|&#34;Range&#34;|&#34;Sum&#34;|&#34;Cumulative Sum&#34;|&#34;% of Total&#34;|&#34;% of Factor&#34;|&#34;% of Grand Total&#34;|&#34;Std Dev&#34;|&#34;Variance&#34;|&#34;Std Err&#34;|&#34;Interquartile Range&#34;|&#34;Median Absolute Deviation&#34;|&#34;First Quartile&#34;|&#34;Third Quartile&#34;=&#34;Mean&#34; );")
        .example("dt = Open(&#34;$SAMPLE_DATA/PopAgeGroup.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Order Statistic( &#34;Max&#34; ), Variables(  X( :&#34;F Rate 0-19&#34;n ), Y( :Region, Order By( :&#34;F Rate 0-19&#34;n, Ascending ) ) ), Elements( Box Plot( X, Y ) ) );"),
    ScriptItem("Back Color", SI_Color, SI_NoMenu, EGraphBuilderBackColorCmd)
        .doc("Sets the color for the entire background around the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Back Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Back Color(&#34;Yellow&#34;);"),
    ScriptItem("Grid Color", SI_Color, SI_NoMenu, EGraphBuilderGridColorCmd)
        .doc("Sets the color for the grid lines on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Grid Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Grid Color(&#34;Red&#34;);"),
    ScriptItem("Grid Transparency", SI_NumProp, SI_NoMenu, EGraphBuilderGridTransparencyCmd)
        .doc("Sets the transparency for the grid lines.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Grid Transparency( fraction=1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Grid Transparency(.2);"),
    ScriptItem("Spacing Borders", SI_BoolProp, SI_NoMenu, EGraphBuilderSpacingBordersCmd)
        .doc("Sets the borders for the internal graph panels.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Spacing Borders( 0|1=0 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Spacing Borders(1);"),
    ScriptItem("Title Fill Color", SI_Color, SI_NoMenu, EGraphBuilderTitleFillColorCmd)
        .doc("Sets the color for the title background fill on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Title Fill Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Fill Color(&#34;Cyan&#34;);"),
    ScriptItem("Title Text Color", SI_Color, SI_NoMenu, EGraphBuilderTitleTextColorCmd)
        .doc("Sets the color for the title text on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Title Text Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Text Color(&#34;Red&#34;);"),
    ScriptItem("Title Frame Color", SI_Color, SI_NoMenu, EGraphBuilderTitleFrameColorCmd)
        .doc("Sets the color for the line around the title frame on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Title Frame Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Frame Color(&#34;Blue&#34;);"),
    ScriptItem("Title Transparency", SI_NumProp, SI_NoMenu, EGraphBuilderTitleTransparencyCmd)
        .doc("Sets the transparency for the title frame on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Title Transparency( fraction=1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Transparency(.2);"),
    ScriptItem("Title Underline", SI_BoolProp, SI_NoMenu, EGraphBuilderTitleUnderlineCmd)
        .doc("Underlines the title or removes the underline on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Title Underline( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Title Frame Color(&#34;Blue&#34;); obj &lt;&lt; Title Underline(1);"),
    ScriptItem("Level Fill Color", SI_Color, SI_NoMenu, EGraphBuilderLevelFillColorCmd)
        .doc("Sets the color for the level names on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Fill Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Fill Color( {103, 214, 214} );"),
    ScriptItem("Level Text Color", SI_Color, SI_NoMenu, EGraphBuilderLevelTextColorCmd)
        .doc("Sets the color for the level name text on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Text Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Text Color(&#34;Red&#34;);"),
    ScriptItem("Level Frame Color", SI_Color, SI_NoMenu, EGraphBuilderLevelFrameColorCmd)
        .doc("Sets the color for the lines around the level names on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Frame Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Frame Color(&#34;Blue&#34;);"),
    ScriptItem("Level Transparency", SI_NumProp, SI_NoMenu, EGraphBuilderLevelTransparencyCmd)
        .doc("Sets the transparency for the level name frame on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Transparency( fraction=1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Transparency(.2);"),
    ScriptItem("Level Spacing Color", SI_Color, SI_NoMenu, EGraphBuilderLevelSpacingColorCmd)
        .doc("Sets the color of the spacing between level labels.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Spacing Color( color );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Spacing Color(&#34;Blue&#34;);"),
    ScriptItem("Level Spacing Transparency", SI_NumProp, SI_NoMenu, EGraphBuilderLevelSpacingTransparencyCmd)
        .doc("Sets the transparency for the spacing between level labels.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Spacing Transparency( fraction=1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Spacing Transparency(.2);"),
    ScriptItem("Level Underline", SI_BoolProp, SI_NoMenu, EGraphBuilderLevelUnderlineCmd)
        .doc("Underlines the level names or removes the underline on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Level Underline( state=0|1 );")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Level Frame Color(&#34;Blue&#34;); obj &lt;&lt; Level Underline(1);"),
    ScriptItem("Page Level Fill Color", SI_Color, SI_NoMenu, EGraphBuilderPageLevelFillColorCmd)
        .doc("Sets the color for the level names on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Level Fill Color( color );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Level Fill Color( {103, 214, 214} );"),
    ScriptItem("Page Level Text Color", SI_Color, SI_NoMenu, EGraphBuilderPageLevelTextColorCmd)
        .doc("Sets the color for the level name text on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Level Text Color( color );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Level Text Color(&#34;Red&#34;);"),
    ScriptItem("Page Level Frame Color", SI_Color, SI_NoMenu, EGraphBuilderPageLevelFrameColorCmd)
        .doc("Sets the color for the lines around the level names on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Level Frame Color( color );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Level Frame Color(&#34;Blue&#34;);"),
    ScriptItem("Page Level Transparency", SI_NumProp, SI_NoMenu, EGraphBuilderPageLevelTransparencyCmd)
        .doc("Sets the transparency for the level name frame on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Level Transparency( fraction=1 );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Level Transparency(.2);"),
    ScriptItem("Page Level Underline", SI_BoolProp, SI_NoMenu, EGraphBuilderPageLevelUnderlineCmd)
        .doc("Underlines the level names or removes the underline on the graph.")
        .helpKey("Graph Builder Customize")
        .prototype("obj &lt;&lt; Page Level Underline( state=0|1 );")
        .example("dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;); obj = dt &lt;&lt; Graph Builder( Variables( X( :Age ), Y( :Height ), Page( :Sex ) ), Elements(Smoother(X,Y))); obj &lt;&lt; Page Level Frame Color(&#34;Blue&#34;); obj &lt;&lt; Page Level Underline(1);"),
    ScriptItem("Relative Sizes", SI_Action, SI_NoMenu | SI_NoScript | SI_NoPref, EGraphBuilderRelativeSizesCmd)
        .helpKey("Graph Builder")
        .prototype("obj &lt;&lt; Relative Sizes;"),
    ScriptItem("Get Legend Server", SI_Action, SI_NoMenu | SI_NoPref, EGraphBuilderGetLegendServerCmd)
        .doc("Returns an object holding information used by the Legend Display and corresponding Display Segs in the graph.")
        .prototype("obj &lt;&lt; Get Legend Server;")
        .example("
		dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;);gb = Graph Builder(Variables(X(:height), Y(:weight), Overlay(:sex), Color(:age)),Elements(Points(X, Y, Legend(1)), Smoother(X, Y, Legend(2))));
		lgnd = gb &lt;&lt; Get Legend Server;items = lgnd &lt;&lt; Get Legend Items; Show(items);"),
    ScriptItem("Get Legend Display", SI_Action, SI_NoMenu | SI_NoPref, EGraphBuilderGetLegendDisplayCmd)
        .doc("Returns the Legend Display Box for the graph that can be queried or modified.")
        .prototype("obj &lt;&lt; Get Legend Display;")
        .example("
		dt = Open(&#34;$SAMPLE_DATA/Big Class.jmp&#34;);gb = Graph Builder(Variables(X(:height), Y(:weight), Overlay(:sex), Color(:age)),Elements(Points(X, Y, Legend(1)), Smoother(X, Y, Legend(2))));
		lgnd = gb &lt;&lt; Get Legend Display;item = lgnd &lt;&lt; Get Item(2, 1); item &lt;&lt; Set Visible ( 0 );"),
    ScriptItem("Done", SI_Action, SI_NoMenu | SI_NoPref, EGraphBuilderDoneCmd)
        .doc("Hides the control panel and turns off any row sampling.")
        .helpKey("Graph Builder Options")
        .prototype("obj &lt;&lt; Done;")
        .example("^LAUNCH_EXAMPLE; obj &lt;&lt; Done;"),
    ScriptItem("", , 0, EGraphBuilderCmd)
        .prototype("obj &lt;&lt; ;"),
    ScriptItem("", , 0, EGraphBuilderCmd)
        .prototype("obj &lt;&lt; ;"),
    ScriptItem("Graph Builder", SI_LaunchLabel, SI_NoMenu, EGraphBuilderGraphBuilderCmd)
        .prototype("obj &lt;&lt; Graph Builder;"),
    ScriptItem::EndOfTable
};

class Trellis : public ScriptableInterface {
public:
    ECategoricalAlternate getCategoricalAlternate() { return mCategoricalAlternate; }
    void setCategoricalAlternate(ECategoricalAlternate value) { mCategoricalAlternate = value; }

    EContinuousAlternate getContinuousAlternate() { return mContinuousAlternate; }
    void setContinuousAlternate(EContinuousAlternate value) { mContinuousAlternate = value; }

    EFitToWindow getFitToWindow() { return mFitToWindow; }
    void setFitToWindow(EFitToWindow value) { mFitToWindow = value; }

    ELegendPosition getLegendPosition() { return mLegendPosition; }
    void setLegendPosition(ELegendPosition value) { mLegendPosition = value; }

    ELinkPageAxes getLinkPageAxes() { return mLinkPageAxes; }
    void setLinkPageAxes(ELinkPageAxes value) { mLinkPageAxes = value; }

    EOrderStatistic getOrderStatistic() { return mOrderStatistic; }
    void setOrderStatistic(EOrderStatistic value) { mOrderStatistic = value; }

    ESubtitleAlignment getSubtitleAlignment() { return mSubtitleAlignment; }
    void setSubtitleAlignment(ESubtitleAlignment value) { mSubtitleAlignment = value; }

    ESubtitleSpan getSubtitleSpan() { return mSubtitleSpan; }
    void setSubtitleSpan(ESubtitleSpan value) { mSubtitleSpan = value; }

    ESummaryStatistic getSummaryStatistic() { return mSummaryStatistic; }
    void setSummaryStatistic(ESummaryStatistic value) { mSummaryStatistic = value; }

    ETitleAlignment getTitleAlignment() { return mTitleAlignment; }
    void setTitleAlignment(ETitleAlignment value) { mTitleAlignment = value; }

    ETitleSpan getTitleSpan() { return mTitleSpan; }
    void setTitleSpan(ETitleSpan value) { mTitleSpan = value; }

    EXGroupEdge getXGroupEdge() { return mXGroupEdge; }
    void setXGroupEdge(EXGroupEdge value) { mXGroupEdge = value; }

    EYGroupEdge getYGroupEdge() { return mYGroupEdge; }
    void setYGroupEdge(EYGroupEdge value) { mYGroupEdge = value; }

    EYGroupLevelOrientation getYGroupLevelOrientation() { return mYGroupLevelOrientation; }
    void setYGroupLevelOrientation(EYGroupLevelOrientation value) { mYGroupLevelOrientation = value; }

    EYGroupTitleOrientation getYGroupTitleOrientation() { return mYGroupTitleOrientation; }
    void setYGroupTitleOrientation(EYGroupTitleOrientation value) { mYGroupTitleOrientation = value; }

    JMPColor getPageLevelTextColor() { return mPageLevelTextColor; }
    void setPageLevelTextColor(JMPColor value) { mPageLevelTextColor = value; }

    JMPColor getLevelFillColor() { return mLevelFillColor; }
    void setLevelFillColor(JMPColor value) { mLevelFillColor = value; }

    JMPColor getPageLevelFillColor() { return mPageLevelFillColor; }
    void setPageLevelFillColor(JMPColor value) { mPageLevelFillColor = value; }

    JMPColor getLevelSpacingColor() { return mLevelSpacingColor; }
    void setLevelSpacingColor(JMPColor value) { mLevelSpacingColor = value; }

    JMPColor getPageLevelFrameColor() { return mPageLevelFrameColor; }
    void setPageLevelFrameColor(JMPColor value) { mPageLevelFrameColor = value; }

    JMPColor getBackColor() { return mBackColor; }
    void setBackColor(JMPColor value) { mBackColor = value; }

    JMPColor getGridColor() { return mGridColor; }
    void setGridColor(JMPColor value) { mGridColor = value; }

    JMPColor getLevelFrameColor() { return mLevelFrameColor; }
    void setLevelFrameColor(JMPColor value) { mLevelFrameColor = value; }

    JMPColor getTitleFrameColor() { return mTitleFrameColor; }
    void setTitleFrameColor(JMPColor value) { mTitleFrameColor = value; }

    JMPColor getLevelTextColor() { return mLevelTextColor; }
    void setLevelTextColor(JMPColor value) { mLevelTextColor = value; }

    JMPColor getTitleTextColor() { return mTitleTextColor; }
    void setTitleTextColor(JMPColor value) { mTitleTextColor = value; }

    JMPColor getTitleFillColor() { return mTitleFillColor; }
    void setTitleFillColor(JMPColor value) { mTitleFillColor = value; }

    TopPtr getLegendFloatingOffset() { return mLegendFloatingOffset; }
    void setLegendFloatingOffset(TopPtr value) { mLegendFloatingOffset = value; }

    bits getIncludeMissingCategories() { return mIncludeMissingCategories; }
    void setIncludeMissingCategories(bits value) { mIncludeMissingCategories = value; }

    bits getContinuousSmoother() { return mContinuousSmoother; }
    void setContinuousSmoother(bits value) { mContinuousSmoother = value; }

    bits getPageLevelUnderline() { return mPageLevelUnderline; }
    void setPageLevelUnderline(bits value) { mPageLevelUnderline = value; }

    bits getShowExcludedRows() { return mShowExcludedRows; }
    void setShowExcludedRows(bits value) { mShowExcludedRows = value; }

    bits getJitter() { return mJitter; }
    void setJitter(bits value) { mJitter = value; }

    bits getLevelUnderline() { return mLevelUnderline; }
    void setLevelUnderline(bits value) { mLevelUnderline = value; }

    bits getAutoStretching() { return mAutoStretching; }
    void setAutoStretching(bits value) { mAutoStretching = value; }

    bits getLockScales() { return mLockScales; }
    void setLockScales(bits value) { mLockScales = value; }

    bits getShowControlPanel() { return mShowControlPanel; }
    void setShowControlPanel(bits value) { mShowControlPanel = value; }

    bits getSpacingBorders() { return mSpacingBorders; }
    void setSpacingBorders(bits value) { mSpacingBorders = value; }

    bits getMissingShapes() { return mMissingShapes; }
    void setMissingShapes(bits value) { mMissingShapes = value; }

    bits getTitleUnderline() { return mTitleUnderline; }
    void setTitleUnderline(bits value) { mTitleUnderline = value; }

    double getGridTransparency() { return mGridTransparency; }
    void setGridTransparency(double value) { mGridTransparency = value; }

    double getTitleTransparency() { return mTitleTransparency; }
    void setTitleTransparency(double value) { mTitleTransparency = value; }

    double getLevelSpacingTransparency() { return mLevelSpacingTransparency; }
    void setLevelSpacingTransparency(double value) { mLevelSpacingTransparency = value; }

    double getGraphHeight() { return mGraphHeight; }
    void setGraphHeight(double value) { mGraphHeight = value; }

    double getGraphWidth() { return mGraphWidth; }
    void setGraphWidth(double value) { mGraphWidth = value; }

    double getCategoricalPointsLimit() { return mCategoricalPointsLimit; }
    void setCategoricalPointsLimit(double value) { mCategoricalPointsLimit = value; }

    double getLevelTransparency() { return mLevelTransparency; }
    void setLevelTransparency(double value) { mLevelTransparency = value; }

    double getContinuousPointsLimit() { return mContinuousPointsLimit; }
    void setContinuousPointsLimit(double value) { mContinuousPointsLimit = value; }

    double getRandomSeed() { return mRandomSeed; }
    void setRandomSeed(double value) { mRandomSeed = value; }

    double getPageCountLimit() { return mPageCountLimit; }
    void setPageCountLimit(double value) { mPageCountLimit = value; }

    double getPageGapSize() { return mPageGapSize; }
    void setPageGapSize(double value) { mPageGapSize = value; }

    double getGraphSpacing() { return mGraphSpacing; }
    void setGraphSpacing(double value) { mGraphSpacing = value; }

    double getSampling() { return mSampling; }
    void setSampling(double value) { mSampling = value; }

    double getPageLevelTransparency() { return mPageLevelTransparency; }
    void setPageLevelTransparency(double value) { mPageLevelTransparency = value; }

    double getSmoothingParameter() { return mSmoothingParameter; }
    void setSmoothingParameter(double value) { mSmoothingParameter = value; }

    static decltype(auto) stateOpcodes() {
        static constexpr std::array opcodes {
            EGraphBuilderCategoricalAlternateCmd,
            EGraphBuilderContinuousAlternateCmd,
            EGraphBuilderFitToWindowCmd,
            EGraphBuilderLegendPositionCmd,
            EGraphBuilderLinkPageAxesCmd,
            EGraphBuilderOrderStatisticCmd,
            EGraphBuilderSubtitleAlignmentCmd,
            EGraphBuilderSubtitleSpanCmd,
            EGraphBuilderSummaryStatisticCmd,
            EGraphBuilderTitleAlignmentCmd,
            EGraphBuilderTitleSpanCmd,
            EGraphBuilderXGroupEdgeCmd,
            EGraphBuilderYGroupEdgeCmd,
            EGraphBuilderYGroupLevelOrientationCmd,
            EGraphBuilderYGroupTitleOrientationCmd,
            EGraphBuilderPageLevelTextColorCmd,
            EGraphBuilderLevelFillColorCmd,
            EGraphBuilderPageLevelFillColorCmd,
            EGraphBuilderLevelSpacingColorCmd,
            EGraphBuilderPageLevelFrameColorCmd,
            EGraphBuilderBackColorCmd,
            EGraphBuilderGridColorCmd,
            EGraphBuilderLevelFrameColorCmd,
            EGraphBuilderTitleFrameColorCmd,
            EGraphBuilderLevelTextColorCmd,
            EGraphBuilderTitleTextColorCmd,
            EGraphBuilderTitleFillColorCmd,
            EGraphBuilderLegendFloatingOffsetCmd,
            EGraphBuilderIncludeMissingCategoriesCmd,
            EGraphBuilderContinuousSmootherCmd,
            EGraphBuilderPageLevelUnderlineCmd,
            EGraphBuilderShowExcludedRowsCmd,
            EGraphBuilderJitterCmd,
            EGraphBuilderLevelUnderlineCmd,
            EGraphBuilderAutoStretchingCmd,
            EGraphBuilderLockScalesCmd,
            EGraphBuilderShowControlPanelCmd,
            EGraphBuilderSpacingBordersCmd,
            EGraphBuilderMissingShapesCmd,
            EGraphBuilderTitleUnderlineCmd,
            EGraphBuilderGridTransparencyCmd,
            EGraphBuilderTitleTransparencyCmd,
            EGraphBuilderLevelSpacingTransparencyCmd,
            EGraphBuilderGraphHeightCmd,
            EGraphBuilderGraphWidthCmd,
            EGraphBuilderCategoricalPointsLimitCmd,
            EGraphBuilderLevelTransparencyCmd,
            EGraphBuilderContinuousPointsLimitCmd,
            EGraphBuilderRandomSeedCmd,
            EGraphBuilderPageCountLimitCmd,
            EGraphBuilderPageGapSizeCmd,
            EGraphBuilderGraphSpacingCmd,
            EGraphBuilderSamplingCmd,
            EGraphBuilderPageLevelTransparencyCmd,
            EGraphBuilderSmoothingParameterCmd
        };
        return opcodes;
    }

    auto tie() {
        return std::tie(
            mCategoricalAlternate,
            mContinuousAlternate,
            mFitToWindow,
            mLegendPosition,
            mLinkPageAxes,
            mOrderStatistic,
            mSubtitleAlignment,
            mSubtitleSpan,
            mSummaryStatistic,
            mTitleAlignment,
            mTitleSpan,
            mXGroupEdge,
            mYGroupEdge,
            mYGroupLevelOrientation,
            mYGroupTitleOrientation,
            mPageLevelTextColor,
            mLevelFillColor,
            mPageLevelFillColor,
            mLevelSpacingColor,
            mPageLevelFrameColor,
            mBackColor,
            mGridColor,
            mLevelFrameColor,
            mTitleFrameColor,
            mLevelTextColor,
            mTitleTextColor,
            mTitleFillColor,
            mLegendFloatingOffset,
            mIncludeMissingCategories,
            mContinuousSmoother,
            mPageLevelUnderline,
            mShowExcludedRows,
            mJitter,
            mLevelUnderline,
            mAutoStretching,
            mLockScales,
            mShowControlPanel,
            mSpacingBorders,
            mMissingShapes,
            mTitleUnderline,
            mGridTransparency,
            mTitleTransparency,
            mLevelSpacingTransparency,
            mGraphHeight,
            mGraphWidth,
            mCategoricalPointsLimit,
            mLevelTransparency,
            mContinuousPointsLimit,
            mRandomSeed,
            mPageCountLimit,
            mPageGapSize,
            mGraphSpacing,
            mSampling,
            mPageLevelTransparency,
            mSmoothingParameter
        );
    }
private:
    ECategoricalAlternate mCategoricalAlternate = ECategoricalAlternate::BoxPlot;
    EContinuousAlternate mContinuousAlternate;
    EFitToWindow mFitToWindow;
    ELegendPosition mLegendPosition;
    ELinkPageAxes mLinkPageAxes;
    EOrderStatistic mOrderStatistic = EOrderStatistic::Mean;
    ESubtitleAlignment mSubtitleAlignment;
    ESubtitleSpan mSubtitleSpan;
    ESummaryStatistic mSummaryStatistic = ESummaryStatistic::Mean;
    ETitleAlignment mTitleAlignment;
    ETitleSpan mTitleSpan;
    EXGroupEdge mXGroupEdge;
    EYGroupEdge mYGroupEdge;
    EYGroupLevelOrientation mYGroupLevelOrientation;
    EYGroupTitleOrientation mYGroupTitleOrientation;
    JMPColor mPageLevelTextColor;
    JMPColor mLevelFillColor;
    JMPColor mPageLevelFillColor;
    JMPColor mLevelSpacingColor;
    JMPColor mPageLevelFrameColor;
    JMPColor mBackColor;
    JMPColor mGridColor;
    JMPColor mLevelFrameColor;
    JMPColor mTitleFrameColor;
    JMPColor mLevelTextColor;
    JMPColor mTitleTextColor;
    JMPColor mTitleFillColor;
    TopPtr mLegendFloatingOffset;
    bits mIncludeMissingCategories = false;
    bits mContinuousSmoother = true;
    bits mPageLevelUnderline = false;
    bits mShowExcludedRows = false;
    bits mJitter = true;
    bits mLevelUnderline = false;
    bits mAutoStretching = true;
    bits mLockScales = false;
    bits mShowControlPanel = true;
    bits mSpacingBorders = false;
    bits mMissingShapes = false;
    bits mTitleUnderline = false;
    double mGridTransparency = 1;
    double mTitleTransparency = 1;
    double mLevelSpacingTransparency = 1;
    double mGraphHeight;
    double mGraphWidth;
    double mCategoricalPointsLimit = 5000;
    double mLevelTransparency = 1;
    double mContinuousPointsLimit = 50000;
    double mRandomSeed;
    double mPageCountLimit = 200;
    double mPageGapSize = 25;
    double mGraphSpacing = 1;
    double mSampling;
    double mPageLevelTransparency = 1;
    double mSmoothingParameter = 0.05;
};