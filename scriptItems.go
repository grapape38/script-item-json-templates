package main

import (
	"fmt"
	"sort"
	"strings"
)

/*ScriptItemType the type of a ScriptItem*/
type ScriptItemType string

/*Script Item Types*/
const (
	Number      ScriptItemType = "number"
	Color       ScriptItemType = "color"
	Font        ScriptItemType = "font"
	Enum        ScriptItemType = "enum"
	Bool        ScriptItemType = "bool"
	Char        ScriptItemType = "char"
	NumSides    ScriptItemType = "sides"
	PenStyle    ScriptItemType = "penStyle"
	Pattern     ScriptItemType = "pattern"
	Top         ScriptItemType = "top"
	NumPoint    ScriptItemType = "numPoint"
	BoolPoint   ScriptItemType = "boolPoint"
	EnumPoint   ScriptItemType = "enumPoint"
	SubTable    ScriptItemType = "subTable"
	ContinTable ScriptItemType = "continTable"
	LaunchLabel ScriptItemType = "launchLabel"
	Action      ScriptItemType = "action"
	Unknown     ScriptItemType = "unknown"
)

/*ScriptItemAttrib a boolean attribute of a ScriptItem, example: NoMenu*/
type ScriptItemAttrib string

/*Script Item Attributes*/
const (
	NoMenu   ScriptItemAttrib = "noMenu"
	Default  ScriptItemAttrib = "default"
	NoScript ScriptItemAttrib = "noScript"
	MenuOnly ScriptItemAttrib = "menuOnly"
	Custom   ScriptItemAttrib = "custom"
	Explicit ScriptItemAttrib = "explicit"
	NoPref   ScriptItemAttrib = "noPref"
)

/*ScriptItem an action, a getter / setter method, or a nested script item table*/
type ScriptItem struct {
	Name         string             `json:"name"`
	Type         ScriptItemType     `json:"type"`
	Doc          string             `json:"doc"`
	Examples     []string           `json:"examples"`
	EnumProps    []string           `json:"enumProps"`
	Prototype    string             `json:"prototype"`
	HelpKey      string             `json:"helpKey"`
	Tip          string             `json:"tip"`
	Attributes   []ScriptItemAttrib `json:"attributes"`
	DefaultValue string             `json:"defaultValue"`
	SubTable     *ScriptItemTable   `json:"subTable"`
}

func (si *ScriptItem) Opcode(tableName string) string {
	return fmt.Sprintf("E%s%sCmd", makeCamelCase(tableName, true), si.commandName())
}

func (si *ScriptItem) EnumNamesString(tableName string) string {
	return fmt.Sprintf("%s%sNames", makeCamelCase(tableName, false), si.fieldName())
}

func (si *ScriptItem) AttribsString() string {
	var words []string
	if len(si.Attributes) == 0 {
		return "0"
	}
	for _, attrib := range si.Attributes {
		words = append(words, attribString(attrib))
	}
	return strings.Join(words, " | ")
}

func attribString(attrib ScriptItemAttrib) string {
	switch attrib {
	case NoMenu:
		return "SI_NoMenu"
	case Default:
		return "SI_Dflt"
	case NoScript:
		return "SI_NoScript"
	case MenuOnly:
		return "SI_MenuOnly"
	case Custom:
		return "SI_Custom"
	case Explicit:
		return "SI_Explicit"
	case NoPref:
		return "SI_NoPref"
	}
	return ""
}

func (si *ScriptItem) enumClassName() string {
	return fmt.Sprintf("E%s", si.fieldName())
}

func (si *ScriptItem) enumValue(i int) string {
	if i < 0 || i >= len(si.EnumProps) {
		return ""
	}
	return fmt.Sprintf("%s", makeCamelCase(cleanScriptString(si.EnumProps[i]), true))
}

func (si *ScriptItem) enumValues() []string {
	var names []string
	for i := 0; i < len(si.EnumProps); i++ {
		names = append(names, si.enumValue(i))
	}
	return names
}

func (si *ScriptItem) getterSetterName(isGetter bool) string {
	var getSet string
	if isGetter {
		getSet = "get"
	} else {
		getSet = "set"
	}
	return fmt.Sprintf("%s%s", getSet, si.fieldName())
}

func (si *ScriptItem) TypeString() string {
	switch si.Type {
	case Number:
		return "SI_NumProp"
	case Color:
		return "SI_Color"
	case Font:
		return "SI_Font"
	case Enum:
		return "SI_EnumProperty"
	case Bool:
		return "SI_BoolProp"
	case Char:
		return "SI_CharProp"
	case NumSides:
		return "SI_NumSides"
	case PenStyle:
		return "SI_PenStyle"
	case Pattern:
		return "SI_Pattern"
	case Top:
		return "SI_TopExpr"
	case NumPoint:
		return "SI_NumPoint"
	case BoolPoint:
		return "SI_BoolPoint"
	case EnumPoint:
		return "SI_EnumPoint"
	case SubTable:
		return "SI_Subtable"
	case ContinTable:
		return "SI_Contintable"
	case Action:
		return "SI_Action"
	case LaunchLabel:
		return "SI_LaunchLabel"
	}
	return ""
}

func (si *ScriptItem) dataType() string {
	switch si.Type {
	case Number:
		return "double"
	case Color:
		return "JMPColor"
	case Font:
		return "GFont"
	case Enum:
		return si.enumClassName()
	case Bool:
		return "bits"
	case Char:
		return "JString"
	case NumSides:
		return "DSides"
	case PenStyle:
		return "PenStyle"
	case Pattern:
		return "GPattern"
	case Top:
		return "TopPtr"
	case NumPoint:
		return "DPoint"
	case BoolPoint:
		return "BoolPt"
	case EnumPoint:
		return fmt.Sprintf("generic_point<%s>", si.enumClassName())
	}
	return ""
}

func (si *ScriptItem) defaultValue() string {
	if len(strings.Trim(si.DefaultValue, " \n\t")) > 0 {
		switch si.Type {
		case Bool:
			return si.DefaultValue
		case Number:
			return si.DefaultValue
		case Char:
			return fmt.Sprintf("\"%s\"", si.DefaultValue)
		case Enum:
			if enumIndex := nameLookup(si.EnumProps, si.DefaultValue); enumIndex != -1 {
				return fmt.Sprintf("%s::%s", si.enumClassName(), si.enumValue(enumIndex))
			}
		}
	}
	return ""
}

func (si *ScriptItem) commandName() string {
	return makeCamelCase(cleanScriptString(si.Name), true)
}

func (si *ScriptItem) fieldName() string {
	return makeCamelCase(skipWord(cleanScriptString(si.Name), "set"), true)
}

func (si *ScriptItem) memberName() string {
	return fmt.Sprintf("m%s", si.fieldName())
}

func (si *ScriptItem) isDataMember() bool {
	return len(si.dataType()) > 0
}

func (si *ScriptItem) hasOpcode() bool {
	return si.Type != SubTable && si.Type != ContinTable && si.Type != Unknown
}

func (si *ScriptItem) asDataMember(tableName string) ClassDataMember {
	return ClassDataMember{
		Type:         si.dataType(),
		Name:         si.memberName(),
		Getter:       si.getterSetterName(true),
		Setter:       si.getterSetterName(false),
		DefaultValue: si.defaultValue(),
		Opcode:       si.Opcode(tableName),
	}
}

/*EnumInfo the strings information for a single Enum Script Item*/
type EnumInfo struct {
	StringsName string
	ClassName   string
	Values      []string
	Strings     []string
}

type traversal int

const (
	preOrder  = 0
	postOrder = 1
)

/*ScriptItemTable a named collection of Script Items*/
type ScriptItemTable struct {
	Name        string       `json:"name"`
	ScriptItems []ScriptItem `json:"scriptItems"`
}

func (table *ScriptItemTable) OpcodeTableName() string {
	return fmt.Sprintf("%sOpcodes", makeCamelCase(table.Name, false))
}

func (table *ScriptItemTable) Opcodes() []string {
	var opcodes []string
	for _, tbl := range table.AllTables(preOrder) {
		for _, item := range tbl.ScriptItems {
			if item.hasOpcode() {
				opcodes = append(opcodes, item.Opcode(table.Name))
			}
		}
	}
	return opcodes
}

func (table *ScriptItemTable) TableName() string {
	return fmt.Sprintf("%sItems", makeCamelCase(table.Name, false))
}

func (table *ScriptItemTable) EnumInfos() []EnumInfo {
	var enumInfos []EnumInfo
	for _, tbl := range table.AllTables(preOrder) {
		for _, item := range tbl.ScriptItems {
			if item.Type == Enum {
				enumInfo := EnumInfo{
					StringsName: item.EnumNamesString(tbl.Name),
					ClassName:   item.enumClassName(),
					Values:      item.enumValues(),
					Strings:     item.EnumProps,
				}
				enumInfos = append(enumInfos, enumInfo)
			}
		}
	}
	return enumInfos
}

func allTablesHelper(cur *ScriptItemTable, order traversal, tables *[]ScriptItemTable) {
	if order == preOrder {
		*tables = append(*tables, *cur)
	}
	for _, item := range cur.ScriptItems {
		if item.SubTable != nil {
			allTablesHelper(item.SubTable, order, tables)
		}
	}
	if order == postOrder {
		*tables = append(*tables, *cur)
	}
}

func (table *ScriptItemTable) AllTables(order traversal) []ScriptItemTable {
	var tables []ScriptItemTable
	allTablesHelper(table, order, &tables)
	return tables
}

/*ClassDataMember a data representation of a single script item*/
type ClassDataMember struct {
	Type         string
	Getter       string
	Setter       string
	Name         string
	DefaultValue string
	Opcode       string
}

/*ScriptableClass a class referencing a Script Item Table*/
type ScriptableClass struct {
	Name                string `json:"name"`
	ScriptItemTableName string `json:"scriptItemTable"`
	Members             []ClassDataMember
}

func (sclass *ScriptableClass) loadMembers(tables []ScriptItemTable) {
	sclass.Members = nil
	for _, tbl := range tables {
		if equalsLoosely(tbl.Name, sclass.ScriptItemTableName) {
			for _, si := range tbl.ScriptItems {
				if si.isDataMember() {
					sclass.Members = append(sclass.Members, si.asDataMember(tbl.Name))
				}
			}
			break
		}
	}
	sort.Slice(sclass.Members, func(i, j int) bool {
		return sclass.Members[i].Type < sclass.Members[j].Type
	})
}

func (si *ScriptItem) printDebug(table *ScriptItemTable) {
	fmt.Println(si.dataType())
	fmt.Println(si.AttribsString())
	fmt.Println(si.Opcode(table.Name))
	fmt.Println(si.getterSetterName(true))
	fmt.Println(si.memberName())
}

func (table *ScriptItemTable) printDebug() {
	fmt.Println(table.Name)
	tables := table.AllTables(preOrder)
	for _, tbl := range tables {
		for _, item := range tbl.ScriptItems {
			item.printDebug(table)
		}
	}
	fmt.Println()
}

func (sclass *ScriptableClass) printDebug() {
	fmt.Println(sclass.Name)
	for _, member := range sclass.Members {
		fmt.Println(member.Name)
	}
}
