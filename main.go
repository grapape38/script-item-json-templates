package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
)

/*ScriptItemTemplateData a list of Script Item Tables and Script Item Classes*/
type ScriptItemTemplateData struct {
	Tables  []ScriptItemTable
	Classes []ScriptableClass
}

const tableDirectory string = "input/scriptItemTables"

func main() {
	var tables []ScriptItemTable

	files, err := ioutil.ReadDir(tableDirectory)
	check(err)

	for _, file := range files {
		f, err := ioutil.ReadFile(tableDirectory + "/" + file.Name())
		check(err)

		var table ScriptItemTable
		if err := json.Unmarshal(stripBOM(f), &table); err != nil {
			fmt.Printf("error parsing %s: %s", file.Name(), formatJSONError(string(f), err))
			return
		}
		tables = append(tables, table)
	}

	/*for _, tbl := range tables {
		debugTable(&tbl)
	}*/
	f, err := ioutil.ReadFile("input/classes.json")
	check(err)

	var classes []ScriptableClass
	if err := json.Unmarshal(stripBOM(f), &classes); err != nil {
		fmt.Printf("error parsing classes.json: %s", err)
		return
	}
	for index := range classes {
		classes[index].loadMembers(tables)
	}

	tmpl, err := template.New("scriptItems.tmpl").ParseFiles("scriptItems.tmpl")
	check(err)

	data := ScriptItemTemplateData{
		Tables:  tables,
		Classes: classes,
	}
	var buf bytes.Buffer
	err = tmpl.Execute(&buf, data)
	check(err)

	err = ioutil.WriteFile("output/scriptableClasses.cpp", buf.Bytes(), 0644)
	check(err)
}
